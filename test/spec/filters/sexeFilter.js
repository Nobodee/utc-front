'use strict';

describe('Filter: sexeFilter', function () {

  // load the filter's module
  beforeEach(module('testUtcApp'));

  // initialize a new instance of the filter before each test
  var sexeFilter;
  beforeEach(inject(function ($filter) {
    sexeFilter = $filter('sexeFilter');
  }));

  it('should return the input prefixed with "sexeFilter filter:"', function () {
    var text = 'angularjs';
    expect(sexeFilter(text)).toBe('sexeFilter filter: ' + text);
  });

});
