'use strict';

describe('Service: Adherent', function () {

  // load the service's module
  beforeEach(module('TestutcApp'));

  // instantiate service
  var Adherent;
  beforeEach(inject(function (_Adherent_) {
    Adherent = _Adherent_;
  }));

  it('should do something', function () {
    expect(!!Adherent).toBe(true);
  });

});
