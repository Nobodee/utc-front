'use strict';

describe('Service: Inscription', function () {

  // load the service's module
  beforeEach(module('TestutcApp'));

  // instantiate service
  var Inscription;
  beforeEach(inject(function (_Inscription_) {
    Inscription = _Inscription_;
  }));

  it('should do something', function () {
    expect(!!Inscription).toBe(true);
  });

});
