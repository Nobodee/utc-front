'use strict';

describe('Service: Categorie', function () {

  // load the service's module
  beforeEach(module('TestutcApp'));

  // instantiate service
  var Categorie;
  beforeEach(inject(function (_Categorie_) {
    Categorie = _Categorie_;
  }));

  it('should do something', function () {
    expect(!!Categorie).toBe(true);
  });

});
