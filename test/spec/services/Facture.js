'use strict';

describe('Service: Facture', function () {

  // load the service's module
  beforeEach(module('TestutcApp'));

  // instantiate service
  var Facture;
  beforeEach(inject(function (_Facture_) {
    Facture = _Facture_;
  }));

  it('should do something', function () {
    expect(!!Facture).toBe(true);
  });

});
