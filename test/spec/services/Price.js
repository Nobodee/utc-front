'use strict';

describe('Service: Price', function () {

  // load the service's module
  beforeEach(module('TestutcApp'));

  // instantiate service
  var Price;
  beforeEach(inject(function (_Price_) {
    Price = _Price_;
  }));

  it('should do something', function () {
    expect(!!Price).toBe(true);
  });

});
