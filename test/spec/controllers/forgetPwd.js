'use strict';

describe('Controller: ForgetpwdCtrl', function () {

  // load the controller's module
  beforeEach(module('testUtcApp'));

  var ForgetpwdCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ForgetpwdCtrl = $controller('ForgetpwdCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
