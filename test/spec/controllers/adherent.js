'use strict';

describe('Controller: AdherentCtrl', function () {

  // load the controller's module
  beforeEach(module('testUtcApp'));

  var AdherentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AdherentCtrl = $controller('AdherentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
