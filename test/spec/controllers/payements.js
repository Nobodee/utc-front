'use strict';

describe('Controller: PayementsCtrl', function () {

  // load the controller's module
  beforeEach(module('testUtcApp'));

  var PayementsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PayementsCtrl = $controller('PayementsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
