'use strict';

describe('Controller: InscriptionCtrl', function () {

  var InscriptionCtrl,
    scope;
  // load the controller's module
  beforeEach(function () {
    angular.mock.module('testUtcApp');
    angular.mock.inject(function ($controller, $rootScope) {
      scope = $rootScope.$new();
      InscriptionCtrl = $controller('InscriptionCtrl', {
        $scope: scope
      })
    })
  });


  it('retourner age', function () {
    var date = new Date("October 17, 1994 11:13:00");

    scope.naissance = date;
    scope.age = null;

    scope.getAge(date);

    expect(scope.age).to.be.equal(21);
  });

});
