'use strict';

angular.module('testUtcApp')
  .filter('sexeFilter', function () {
    function getSexe(sexe){
      var s;
      if(sexe == 'h'){
        s = 'Masculin';
      } else {
        s = 'Féminin';
      }
      return s;
    }

    return function (sexe) {
      return getSexe(sexe);
    };
  });
