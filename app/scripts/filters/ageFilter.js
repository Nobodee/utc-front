'use strict';

angular.module('testUtcApp')
  .filter('ageFilter', function() {
       function calculateAge(naissance) { // birthday is a date
            var birthday = new Date(naissance);
           var ageDifMs = Date.now() - birthday.getTime();
           var ageDate = new Date(ageDifMs); // miliseconds from epoch
           var age = Math.abs(ageDate.getUTCFullYear() - 1970);
            age = age + ' ans';
           if (!age){
             age = 'Inconnu'
           }
           return age;
       }
    
       return function(birthdate) {
             return calculateAge(birthdate);
       };
    })
    
    .filter('ageFilterNum', function() {
       function calculateAge(naissance) { // birthday is a date
            var birthday = new Date(naissance);
           var ageDifMs = Date.now() - birthday.getTime();
           var ageDate = new Date(ageDifMs); // miliseconds from epoch
           var age = Math.abs(ageDate.getUTCFullYear() - 1970);
           if (!age){
             age = 'Inconnu'
           }
           return age;
       }
    
       return function(birthdate) {
             return calculateAge(birthdate);
       };
    });


