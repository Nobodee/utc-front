'use strict';

angular.module('testUtcApp')
  .factory('Inscription', ['$resource', 'envService', function ($resource, envService) {

    // Public API here
    var apiUrl = envService.read('apiUrl');

    return $resource(apiUrl + 'api/inscription/');
  }]);
