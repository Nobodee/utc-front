'use strict';

angular.module('testUtcApp')
  .factory('Facture', ['$resource', 'envService', function ($resource, envService) {

    var apiUrl = envService.read('apiUrl');

    var facReg = $resource(apiUrl + 'api/facture-reg/');
    var avoir = $resource(apiUrl + 'api/avoir/');
    var pay = $resource(apiUrl + 'api/payement/:pay', {pay:'@id'});
      var especeTotal = $resource(apiUrl + 'api/espece/');
      var cheque = $resource(apiUrl + 'api/cheque/');
      var caisse = $resource(apiUrl + 'api/caisse/');
      var depenses = $resource(apiUrl + 'api/depenses/');
      var depot = $resource(apiUrl + 'api/depot/');
    var factures = $resource(apiUrl + 'api/factures/:facture', {facture:'@id'});
    var facturePdf = $resource(apiUrl + 'api/facturePdf/:facture', {facture:'@id'},
      {
        pdf: {
          method: 'GET',
          headers: {
            accept: 'application/pdf'
          },
          responseType: 'arraybuffer',
          cache: true,
          transformResponse: function (data) {
            console.log(data);
            var pdf;
            if (data) {
              pdf = new Blob([data], {
                type: 'application/pdf'
              });
            }
            return {
              response: pdf
            };
          }
        }
      }
    );

    // Public API here
    return {
      facReg: facReg,
      pay: pay,
      avoir: avoir,
      factures: factures,
      facturePdf: facturePdf,
      caisse: caisse,
      depot: depot, 
      depenses: depenses, 
        especeTotal: especeTotal,
        cheque: cheque
    };
  }]);
