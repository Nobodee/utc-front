'use strict';

angular.module('testUtcApp')
  .service('Account', ['$resource', 'envService', function ($resource, envService) {

    var apiUrl = envService.read('apiUrl');
    
    var accounts = $resource(apiUrl + 'api/accounts');

    // Public API here
    return {
      accounts: accounts
    };
    
  }]);
