'use strict';

angular.module('testUtcApp')
  .factory('Article', ['$resource', 'envService', function ($resource, envService) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var apiUrl = envService.read('apiUrl');

      
   
    var articles = $resource(apiUrl + 'api/articles/:sort', {sort:'@id'});
    var edit = $resource(apiUrl + 'api/edit_articles');
    var sorts = $resource(apiUrl + 'api/sorts');
    var vente = $resource(apiUrl + 'api/vente/:facture', {facture:'@id'});

    // Public API here
    return {
        sorts: sorts,
        edit: edit,
        vente: vente,
        articles: articles
    };
  }]);
