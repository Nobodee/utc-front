'use strict';

angular.module('testUtcApp')
  .factory('Inscription', ['$resource', function ($resource) {

    // Public API here
    return $resource('api/inscription/');
  }]);
