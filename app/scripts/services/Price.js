'use strict';

angular.module('testUtcApp')
  .factory('Price', ['$resource', 'envService', function Price($resource, envService) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var apiUrl = envService.read('apiUrl');


    return $resource(apiUrl + 'api/categories/');

  }]);
