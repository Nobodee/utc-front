'use strict';

angular.module('testUtcApp')
  .factory('Categorie', ['$resource', 'envService', function ($resource, envService) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var apiUrl = envService.read('apiUrl');

    var cat = $resource(apiUrl + 'api/all-categories/');
    var day = $resource(apiUrl + 'api/day-categorie/');
    var rang = $resource(apiUrl + 'api/ranking');



    return {
      cat: cat,
      day: day,
      rang: rang
    };

  }]);
