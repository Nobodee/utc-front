'use strict';

angular.module('testUtcApp')
  .factory('Adhesion', ['$resource', 'envService', function ($resource, envService) {
    // Service logic
    // ...http:/localhost\:8000/

    var apiUrl = envService.read('apiUrl');

    var all = $resource(apiUrl + 'api/adherents/');
    var role = $resource(apiUrl + 'api/assign-role/');
    var my = $resource(apiUrl + 'api/own-adherent/');
    var one = $resource(apiUrl + 'api/adherent/');
    var rem = $resource(apiUrl + 'api/up-remarque-aff/');
    var aff = $resource(apiUrl + 'api/inscription-affectation/');
    var lic = $resource(apiUrl + 'api/license/');
    var update = $resource(apiUrl + 'api/inscription-update-dossier/');
    var up = $resource(apiUrl + 'api/update/');
    var rattachement = $resource(apiUrl + 'api/attach-family/');
    var cFamily = $resource(apiUrl + 'api/create-family/');
    var gFamily = $resource(apiUrl + 'api/get-family/');
    var validate = $resource(apiUrl + 'api/validate/');
    var updateUser = $resource(apiUrl + 'api/update-user/');
    var changePassword = $resource(apiUrl + 'api/change_password/');


    // Public API here
    return {
      all: all,
      my: my,
      rem: rem,
      aff: aff,
      lic: lic,
      cFamily: cFamily,
      gFamily: gFamily,
      rattachement: rattachement,
      one: one,
      update: update,
      up: up,
      validate: validate,
      role: role,
      updateUser: updateUser,
      changePassword: changePassword
    }
  }]);
