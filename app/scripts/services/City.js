'use strict';

angular.module('testUtcApp')
  .factory('City', function (envService, $resource) {
    // Service logic
    // ...

    var apiUrl = envService.read('apiUrl');

    var cities = $resource(apiUrl + 'api/cities/:cp', {cp:'@cp'});



    return {
      cities: cities
    };
  });
