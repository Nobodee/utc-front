'use strict';

angular.module('testUtcApp')
  .controller('NewordersCtrl', function ($scope, $stateParams, Adherent, Categorie, Inscription) {
    Adherent.one.get({id: $stateParams.userId}, function (data) {
      console.log(data.user);
      $scope.user = data.user;
      $scope.registraion = data.user.latest_reg;

      var birthdate = new Date(data.user.naissance);
      $scope.age = moment().diff(birthdate, 'years');
    }, function (err){
      console.log(err);
    });

    $scope.registrationUpdate = {};

    $scope.submitPreAffectation = function (){
      $scope.registrationUpdate.statut = "pre-affectation";
      $scope.registrationUpdate.id = $scope.registraion.id;
      console.log($scope.registrationUpdate);
      console.log($scope.registrationUpdate.category);
      Inscription.save($scope.registrationUpdate);

    };

    console.log($scope.currentUser);
    $scope.categories = [] ;
    Categorie.get(function (data) {
      console.log(data);
      $scope.categories = data.cat;
    });

  });
