'use strict';

angular.module('testUtcApp')
  .controller('GrandlivreCtrl', function ($scope,  Facture)
  {

	  $scope.ecritures = [];
	  getEcriture();

	  function getEcriture() {
		  $scope.ecritures = [];
		  Facture.depenses.get(function (data) {
			  $scope.ecritures.push.apply($scope.ecritures, data.depenses);
		  });
		  Facture.pay.query(function(data){
			  var newArray = [];
			  angular.forEach(data, function(key){
				  key.date = key.date_encaissement;
				  newArray.push(key);
			  });
			  $scope.ecritures.push.apply($scope.ecritures, newArray);
		  });
	  }

	  $scope.makeFiltre = function () {
		  $scope.ecritures = [];
		  Facture.depenses.get($scope.filtre, function (data) {
			  $scope.ecritures.push.apply($scope.ecritures, data.depenses);
		  });
		  Facture.pay.query($scope.filtre, function (data) {
			  var newArray = [];
			  angular.forEach(data, function(key){
				  key.date = key.date_encaissement;
				  newArray.push(key);
			  });
			  $scope.ecritures.push.apply($scope.ecritures, newArray);
		  });
	  };



  });