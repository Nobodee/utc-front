'use strict';

angular.module('testUtcApp')
  .controller('PayementsCtrl', function ($scope, $stateParams, Article, Facture, $state) {
      var factureId = $stateParams.factureId;
      $scope.userId = $stateParams.userId;

      $scope.newPayement = {};

      Article.vente.get({facture: factureId}, function (data) {
          $scope.factures = data;
      });

      $scope.getTotalLines = function (){
          var total = 0;
          angular.forEach($scope.factures.lines, function(key, value){
              total += key.amount;
          });

          return total;

      };

      $scope.getTotalPay = function (){
          var total = 0;
          angular.forEach($scope.factures.payements, function(key, value){
              total += key.amount;
          });

          return total;

      };

      $scope.addPayement = function () {
          console.log($scope.factures.id);
          $scope.newPayement.factureId = $scope.factures.id;

          console.log($scope.newPayement);

          Facture.pay.save($scope.newPayement, function(success){
              $scope.factures = success.fac;
              $scope.newPayement.remarque = "";
              $scope.newPayement.amount = "";
              $scope.newPayement.methodId = "0";
              $scope.newPayement.dateEnc = null;
              $scope.newPayement.dateEch = null;

          }, function(e){
              console.log(e);
          });

      };

      $scope.removeFromPayement = function(id){
          //$scope.familyMembers.splice(index,1);
          Facture.pay.remove({payId: id}, function(success){
              $scope.factures = success.fac;
          }, function(e){
              console.log(e);
          });
      };

      $scope.createAvoir = function(){
          $scope.showDeletePay = false;

          if($scope.getTotalPay() === 0){
              Facture.avoir.save({factureId: factureId, userId: $scope.userId}, function(success){
                  $state.go('app.index.adherent', {userId: $scope.userId});

              }, function (error) {
                  $scope.showFacError = true;
                  console.error(error);
              })
          } else {
              $scope.showDeletePay = true;
          }
         
      }


  });
