'use strict';

angular.module('testUtcApp')
  .controller('InscriptionCtrl', function ($scope, Price, Categorie, Inscription, City, $window, envService, moment) {

    $scope.age = null;
    var mD = moment().subtract(3, 'years');
    $scope.threeYearsAgo = mD.toString();
    console.log($scope.threeYearsAgo);
    var staticUrl = envService.read('staticUrl');
    $scope.citiesUrl = staticUrl + 'cities?searchterm=';

    // $scope.inscriptionsForm.auth_coor.unauthorized = false;

    $scope.inscriptionsForm = {
      contacts: [
        {tel: "", lien_parente: "", nom: "", position: 1}
      ],
      ownTelContacts: [
        {tel: ""}
      ],
      ownEmailContacts: [
        {email: ""}
      ]
      // auth_coor : [{
      //   unauthorized : false
      // }]
    };

    $scope.rang = Categorie.rang.query();

    $scope.error = {
      empty: true
    };
    $scope.naissance = {};
    $scope.categories = [];
    $scope.reductions = [];
    $scope.isLocalReduc = 0;
    $scope.today = new Date();
    $scope.reduc = [];
    var nberContact = 2;

    $scope.getVille = function () {
      var villeId = $scope.ville;
      $scope.changeErrorState('ville');
      console.log(villeId);
      if(villeId){
       // var villeId = ville.id;
        $scope.inscriptionsForm.ville = villeId;
        if(villeId == 11835){
          $scope.isLocalReduc = 30;
        } else {
          $scope.isLocalReduc = 0;
        }
      }
    };

    $scope.cities = [];

    $scope.codePostal = function(cp){
      if(cp.length >= 5){
        City.cities.query({cp: cp}, function(cities){
          console.log(cities);
          $scope.cities = cities;
        });
      }

    };

    $scope.getReduc = function (){
      if($scope.isLocalReduc == 0 && ($scope.reduc == 1 || 2)){
        $scope.isLocalReduc = 20;
      } else if ($scope.isLocalReduc != 30 && $scope.reduc == 0){
        $scope.isLocalReduc = 0;
      }
      console.log($scope.isLocalReduc);
    };

    $scope.uncheckedOther = function (unchecked){
      $scope.changeErrorState('auth_coor');
      if(unchecked && $scope.inscriptionsForm.auth_coor.unauthorized){
        $scope.inscriptionsForm.auth_coor = {
          unauthorized : true
        };
      } else if (unchecked && !$scope.inscriptionsForm.auth_coor.unauthorized && !$scope.inscriptionsForm.auth_coor.coor_p_fft && !$scope.inscriptionsForm.auth_coor.coor_p_tcu && !$scope.inscriptionsForm.auth_coor.coor_jr){
        $scope.inscriptionsForm.auth_coor.unauthorized = true;
      } else if (!unchecked && !$scope.inscriptionsForm.auth_coor.unauthorized && !$scope.inscriptionsForm.auth_coor.coor_p_fft && !$scope.inscriptionsForm.auth_coor.coor_p_tcu && !$scope.inscriptionsForm.auth_coor.coor_jr) {
        $scope.inscriptionsForm.auth_coor.unauthorized = true;
      } else {
        $scope.inscriptionsForm.auth_coor.unauthorized = false;
      }
      console.log($scope.inscriptionsForm.auth_coor);

    };

    $scope.changeErrorState = function(err){
      if(err in $scope.error){
        $scope.error[err] = false;
      }
    };

    $scope.submit = function () {

      $scope.isSubmit = true;
      $scope.error = {};
      var errorField = {};

      $scope.inscriptionsForm.age = $scope.age;
      console.log($scope.inscriptionsForm);

      Inscription.save($scope.inscriptionsForm,
      function(data){
        $window.alert("Votre préinscription a été enregistrée, veuillez consulter votre email.");
        $window.location.href = 'http://www.club.fft.fr/tennisclubdelunion/';
        $scope.isSubmit = false;

      }, function(error){
        $scope.isSubmit = false;
        var err = error.data.error;
        console.log(err);
        if(err.prenom != null){
          errorField.prenom = 'Veuillez saisir votre nom et prénom  !';
        }
        if(err.nom != null){
          errorField.nom = 'Veuillez saisir votre nom !';
        }
        if(err.naissance != null){
          errorField.naissance = 'Veuillez sélectionner votre date de naissance !';
        }
        if(err.sexe != null){
          errorField.sexe = 'Veuillez choisir votre sexe !';
        }
        if(err.ville != null){
          errorField.ville = 'Veuillez selectioner votre ville !';
        }
        if(err['ownTelContacts.0.tel'] != null){
          errorField.tel = 'Veuillez saisir au moins un numéro de téléphone !';
        }
        if(err['ownEmailContacts.0.email'] != null){
          errorField.email = 'Veuillez saisir au moins une adresse email !';
        }
        if(err.auth_coor != null){
          errorField.auth_coor = 'Veuillez indiquer les autorisations concernant l\'usage de vos données personneles svp !';
        }
        if(err['reduction.id'] != null){
          errorField.reduction = 'Veuillez sélectionner une réduction svp !';
        }
        if(err['categorie.id'] != null){
          errorField.categorie = 'Veuillez choisir la formule souhaitée pour votre inscription svp !';
        }


        console.log(errorField);
        $scope.error = errorField;
        $scope.isSubmit = false;
      });
    };

    $scope.addContact = function (date) {
      nberContact += 1;
      $scope.inscriptionsForm.contacts.push({tel: "", lien_parente: "", nom: "", position: nberContact});
    };

    $scope.addTelOwnContact = function () {
      $scope.inscriptionsForm.ownTelContacts.push({tel: ""});
    };

    $scope.addEmailOwnContact = function () {
      $scope.inscriptionsForm.ownEmailContacts.push({email: ""});
    };

    $scope.getAge = function (date) {
      var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
      console.log(birthdate);
      var d = new Date();
      var age = moment().diff(birthdate, 'years');
      console.log(age);
      $scope.age = age;
      $scope.inscriptionsForm.naissance = birthdate;
      $scope.inscriptionsForm.categorie = null;
      $scope.inscriptionsForm.day = null;
      Price.get({age: age}, function (data) {
        console.log(data);
        $scope.categories = data.cat;
        $scope.reductions = data.reduc;
      });
    };
  });
