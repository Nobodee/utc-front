'use strict';

angular.module('testUtcApp')
  .controller('ArticlesCtrl', function ($scope,  RoleStore, Article)
  {
      getArticle();

    $scope.article = {};
    $scope.articleUp = {};
      $scope.today = new Date().toString();

    $scope.EditLine = [];
      
    $scope.edit = function (i) {
        $scope.EditLine[i] = true;
        console.log(i);
    };

      $scope.cancelEdit = function (i) {
          console.log(i);
          $scope.EditLine[i] = false;
      };

      $scope.saveEdit = function (id, i) {
          $scope.articleUp[i].id = id;
          var upArray = $scope.articleUp[i];
          Article.edit.save(upArray, function (success) {
              $scope.EditLine[i] = false;
              $scope.articleUp[i] = {};
              getArticle();
          })
      };

      $scope.remove = function (id) {
          Article.articles.delete({id: id}, function (success) {
              getArticle();
          })
      };

    function getArticle(){
        Article.articles.query(function(data){
            console.log(data);
            $scope.articles = data;
        });
    };

      Article.sorts.query(function(data){
          console.log(data);
          $scope.sorts = data;
      });

      $scope.submit = function () {
          console.log($scope.article);
          Article.articles.save($scope.article,
              function (success) {
                  $scope.article = {
                      dateF: null,
                      dateD: null
                  };
                  getArticle();
                  console.log('success');
              },
              function (err) {
                  console.log(err)
              });
      };




    console.log("fin controlleur ArticlesCtrl");
  });
