'use strict';

angular.module('testUtcApp')
  .controller('ForgetpwdCtrl', ['$scope', 'Adhesion', '$state','$window', function ($scope, Adhesion, $state, $window) {
      $scope.form = {
          date: null,
          prenom: "",
          nom: "",
          password: "",
          confirmPassword: ""
      };

      $scope.error = null;

      $scope.changePassword = function () {
          var form = $scope.form;
          $scope.error = null;
          if(form.password.length > 5){
              if(form.password === form.confirmPassword){
                  Adhesion.changePassword.save(form,
                      function (success) {
                          $window.alert("Votre mot de passe à été mis à jour avec succés.");
                          $state.go('app.auth');
                      },
                      function (error) {
                          $scope.error = error.data.error;
                      }
                  );
              } else {
                  $scope.error = "Mot de passe non identique";
                  console.log("mot de passe non identique")
              }
          } else {
              $scope.error = "Le mot de passe doit contenir au moins 6 caractères";
              console.log("Le mot de passe doit contenir au moins 6 caractères")
          }
      }
  }]);
