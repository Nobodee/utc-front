'use strict';

angular.module('testUtcApp')
  .controller('NewventeCtrl', function ($scope,  RoleStore) 
  {
  	$scope.age = null;
    $scope.unauthorized = true;

    $scope.inscriptionsForm = {
      contacts: [
        {tel: "", lien_parente: "", nom: "", position: 1},
        
      ]
    };
    $scope.naissance = {};
    $scope.categories = [];
    $scope.reductions = [];
    $scope.isLocalReduc = 0;
    $scope.today = new Date();
    $scope.reduc = [];
    var nberContact = 2;

    $scope.getVille = function (ville) {
      if(ville){
        var villeId = ville.originalObject.id;
        $scope.inscriptionsForm.ville = villeId;
        if(villeId == 11835){
          $scope.isLocalReduc = 30;
        } else {
          $scope.isLocalReduc = 0;
        }
      }
    };

    $scope.getReduc = function (){
      if($scope.isLocalReduc == 0 && ($scope.reduc == 1 || 2)){
        $scope.isLocalReduc = 20;
      } else if ($scope.isLocalReduc != 30 && $scope.reduc == 0){
        $scope.isLocalReduc = 0;
      }
      console.log($scope.isLocalReduc);
    };

    $scope.uncheckedOther = function (unchecked){
      console.log($scope.inscriptionsForm.auth_coor);
      if(unchecked && $scope.unauthorized){
        $scope.inscriptionsForm.auth_coor = {};
      } else if (!unchecked) {
        $scope.unauthorized = false;
      }
    }

    $scope.submit = function () {

      $scope.inscriptionsForm.age = $scope.age;
      console.log($scope.inscriptionsForm);

      Inscription.save($scope.inscriptionsForm);
    };

    $scope.addContact = function (date) {
      nberContact += 1;
      $scope.inscriptionsForm.contacts.push({tel: "", lien_parente: "", nom: "", position: nberContact});
    };

    $scope.getAge = function (date) {
      var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
      console.log(birthdate);
      var d = new Date();
      var age = moment().diff(birthdate, 'years');
      console.log(age);
      $scope.age = age;
      $scope.inscriptionsForm.naissance = birthdate;
      Price.get({age: age}, function (data) {
        console.log(data);
        $scope.categories = data.cat;
        $scope.reductions = data.reduc;
      });
    };

  

  });