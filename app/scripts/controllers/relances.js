'use strict';

angular.module('testUtcApp')
  .controller('RelancesCtrl', function ($scope,  RoleStore) 
  {

   $scope.factures =  [
							    {
							   
								'date' :'2016-06-01',
								'numfacture' :'20160501',
								
								'designation' :'Inscription Pierre Bruneaud',
								'nom' :'Durand',
								'prenom' :'Pierre',
								
								'montant' :255,
								'encours' :200,
								'solde' :55,
									'relance1':'12/03/2016',
									'relance2':'12/04/2016',
								'id_facture' :1
								
								},
								 {
							  
								'date' :'2016-07-01',
								'numfacture' :'20160501',
								'designation' :'Inscription Anne Dubois',
								'nom' :'Dubois',
								'prenom' :'Anne',
								'montant' :255,
								'encours' :200,
								'solde' :55,
									'relance1':'12/03/2016',
									'relance2':'12/04/2016',
								'id_facture' :1
								
								}
							] ;

  });