'use strict';

angular.module('testUtcApp')
  .controller('EditCtrl', function ($scope, Adhesion, Categorie, $stateParams, $state, RoleStore) {

    var userId = $stateParams.userId;
    $scope.inscriptionsForm = {};
    $scope.lastReg = {
      rankings : {id: ""}
    };

    $scope.rang = Categorie.rang.query(function(){
      $scope.inscriptionsForm.ranking = $scope.lastReg.rankings.id;
    });

    $scope.getUser = function(){
      var field = {};

      Adhesion.one.get({id: userId},function(data){
        console.log(data.user);
        $scope.user = data.user;
        $scope.lastReg = data.user.latest_reg;


        field.ownEmailContacts = data.user.latest_reg.own_email;
        field.ownTelContacts = [];
        field.license = data.user.license;
        field.oldClub = data.user.latest_reg.old_club;

        if(data.user.latest_reg.rankings != null){
          field.ranking = data.user.latest_reg.rankings.id+"";
        }


        angular.forEach(data.user.latest_reg.own_tel, function (key, value) {
          var a = {tel: '0'+key.tel};
          this.push(a);
        }, field.ownTelContacts);


        $scope.inscriptionsForm = field;
        console.log($scope.inscriptionsForm);
      });
    };

    $scope.getUserMy = function(){
      var field = {};

      Adhesion.my.get(function(data){
        console.log(data.user);
        $scope.user = data.user;
        $scope.lastReg = data.user.latest_reg;


        field.ownEmailContacts = data.user.latest_reg.own_email;
        field.ownTelContacts = [];
        field.license = data.user.license;
        field.oldClub = data.user.latest_reg.old_club;

        if(data.user.latest_reg.rankings != null){
          field.ranking = data.user.latest_reg.rankings.id+"";
        }

        angular.forEach(data.user.latest_reg.own_tel, function (key, value) {
          var a = {tel: '0'+key.tel};
          this.push(a);
        }, field.ownTelContacts);


        $scope.inscriptionsForm = field;
        console.log($scope.inscriptionsForm);
      });
    };

    $scope.addTelOwnContact = function () {
      $scope.inscriptionsForm.ownTelContacts.push({tel: ""});
    };

    $scope.removeTel = function (index) {
      console.log(index);
      $scope.inscriptionsForm.ownTelContacts.splice(index,1);
    };

    $scope.addEmailOwnContact = function () {
      $scope.inscriptionsForm.ownEmailContacts.push({email: ""});
    };

    var notOnly = false;
    var actRole = RoleStore.getStore();


    angular.forEach(actRole, function (key, value) {
      console.log(value == 'Admin');
      if(value == 'Admin' || value == 'Trésorier' || value == 'Prof' || value == 'Permanent'){
        notOnly = true
      }
    });

    if(notOnly == true) {
      $scope.getUser();
    } else {
      $scope.getUserMy();
    }

    $scope.submit = function () {

      $scope.inscriptionsForm.userId = $scope.user.id;
      Adhesion.updateUser.save($scope.inscriptionsForm, function () {

        $state.go('app.index.adherent', {userId: $scope.user.id});
      })
    }

  });
