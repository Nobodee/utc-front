'use strict';

angular.module('testUtcApp')
  .controller('AppCtrl', function ($scope, $cookies, $auth, $state, RoleStore) {


    $scope.currentUser = {};
    $scope.loginError = false;


    if($cookies.getObject('currentUser') != null){
      $scope.currentUser = $cookies.getObject('currentUser');
      $scope.currentRoles = $cookies.getObject('role')
    }

    $scope.login = function(pseudo, password) {
      $scope.loginError = false;
      var credentials = {
        pseudo: pseudo,
        password: password
      };

      // Use Satellizer's $auth service to login
      $auth.login(credentials).then(function(data) {
        var data = data.data;
        console.log(data.role);
        RoleStore.clearStore();
        angular.forEach(data.role, function(key, value){
          RoleStore.defineRole(key.name, []);
        });
        $cookies.putObject('role', data.role);
        $cookies.putObject('currentUser', data.currentUser);
        var actRole = RoleStore.getStore();
        console.log(actRole);
        $scope.currentUser = data.currentUser;
        $scope.currentRoles = data.role;
        $state.go('app.index.adherent', {userId: data.currentUser.id});
        console.log('redirection');


        //$scope.currentUser = data.currentUser;
        // If login is successful, redirect to the users state
      }, function (err) {
        $scope.loginError = true;
      });
    }
  });
