'use strict';

angular.module('testUtcApp')
  .controller('RecettesCtrl', function ($scope,  RoleStore, Facture, envService, $window, FileSaver, Blob)
  {

    Facture.factures.get(function(data){
      console.log(data);
      $scope.factures=data.factures;

    }, function(err){
      console.error(err);
    });
//////////////////////////// total à regler facture
    $scope.getTotalLines = function (facture){
      var total = 0;
      angular.forEach(facture.lines, function(key, value){
        total += key.amount;
      });
      //console.log(total);
      return total;

    };

    $scope.makeFiltre = function (pdf) {
      console.log(pdf);
      $scope.filtre.getPdf = pdf;

      Facture.factures.get($scope.filtre, function(data){
        console.log(data);
        if(pdf == 1){
         /* var file = new Blob([data], {type: 'application/pdf'});
          var pdfFileUrl = URL.createObjectURL(file);*/
          //FileSaver.saveAs(file, 'facture.pdf');
          var apiUrl = envService.read('staticUrl');

          var url = apiUrl + 'download/'+data.path;
          $window.open(url);
          return;
        }
        $scope.factures=data.factures;

      }, function(err){
        console.error(err);
      });
    };

    $scope.showFacture = function(facture){
      console.log('facture id', facture);
      var id = facture;

      var apiUrl = envService.read('staticUrl');

      var url = apiUrl + 'api/facturePdf/'+id;

      $window.open(url);

      /*Facture.facturePdf.pdf({facture: id}, function(data){
        console.log(data);
        var file = new Blob([data], {type: 'application/pdf'});
        var pdfFileUrl = URL.createObjectURL(file);
        //FileSaver.saveAs(file, 'facture.pdf');
        //$window.open(pdfFileUrl);
      });*/

      //var file = new Blob([data], {type: 'application/pdf'});

     // var file = Facture.facturePdf.pdf({facture: id});
      //var pdfFileUrl = URL.createObjectURL(file);
      // FileSaver.saveAs(file, 'facture.pdf');
      //$window.open(pdfFileUrl);

    };
///////////////////////// encours total facture
    $scope.getTotalPay = function (facture){
      var total = 0;
      angular.forEach(facture.payements, function(key, value){
        total += key.amount;
      })
      //sconsole.log(total);
      return total;

    }


   // var factureId = 15;

  //  Facture.factures.get({facture: factureId}, function(data){
  //    console.log(data);
   //   $scope.lines=data.lines;


  //  }, function(err){
  //    console.error(err);
 //   });




  });
