'use strict';

angular.module('testUtcApp')
  .controller('AccountCtrl', function ($scope, Account) {

     getAccount();

      $scope.account = {};
      /*$scope.articleUp = {};
      $scope.today = new Date().toString();

      $scope.EditLine = [];

      $scope.edit = function (i) {
          $scope.EditLine[i] = true;
          console.log(i);
      };

      $scope.cancelEdit = function (i) {
          console.log(i);
          $scope.EditLine[i] = false;
      };

      $scope.saveEdit = function (id, i) {
          $scope.articleUp[i].id = id;
          var upArray = $scope.articleUp[i];
          Article.edit.save(upArray, function (success) {
              $scope.EditLine[i] = false;
              $scope.articleUp[i] = {};
              getArticle();
          })
      };
       */

      $scope.remove = function (id) {
          Account.accounts.delete({id: id}, function (success) {
              getAccount();
          })
      };
      
      
      function getAccount(){
          Account.accounts.query(function(data){
              console.log(data);
              $scope.accounts = data;
          });   
      }
      
      $scope.submit = function () {
          var accForm = $scope.account;
          console.log(accForm.numero.toString().length);
          $scope.error = null;
          if(accForm.numero.toString().length >= 4){
             if(accForm.numero.toString().startsWith("0") || accForm.numero.toString().startsWith("9")){
                $scope.error = "Le numéro de compte ne doit pas commencer par 0 ou 9"
             } else {
                 if(accForm.titre.length > 0){
                     Account.accounts.save($scope.account,
                         function (success) {
                             $scope.account = {};
                             getAccount();
                             console.log('success');
                         },
                         function (err) {
                             console.log(err);
                             $scope.error = err.data.error.numero[0];
                         });
                 } else {
                     $scope.error = "Veuillez renseigné un libélé pour ce compte"
                 }

             }
          } else {
              $scope.error = "Le numéro de compte doit contenir entre 4 et 6 chiffre"
          }
      };




      console.log("fin controlleur AccountCtrl");


  });
