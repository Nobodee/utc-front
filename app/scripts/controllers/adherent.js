'use strict';

angular.module('testUtcApp')
  .controller('AdherentCtrl',['RoleStore', '$scope', 'Price', 'Adhesion', 'Categorie', 'Article', '$stateParams', 'Facture', 'envService', '$window', '$state', function (RoleStore,$scope, Price, Adhesion, Categorie, Article, $stateParams, Facture, envService, $window, $state) {

    var userId = $stateParams.userId;
    var notOnly = false;

    var actRole = RoleStore.getStore();
    var staticUrl = envService.read('staticUrl');
    var familyId, regId;
    
   /* Article.vente.query({userId: userId}, function (data) {
      $scope.ventes = data;
    });*/

    $scope.getMontant = function(l){
      var total = 0;
      angular.forEach(l, function (key, value) {
        total += key.amount;
      });
      return total;
    };

    $scope.date = function (d) {
      var date = new moment(d).toDate();
      return date;
    };

    $scope.getEncours = function(p){
      var total = 0;
      angular.forEach(p, function (key, value) {
        total += key.amount;
      });
      return total;
    };


    $scope.showFacture = function(facture) {
      console.log('facture id', facture);
      var id = facture;

      var url = staticUrl + 'api/facturePdf/' + id;

      $window.open(url);
    };

    $scope.showDossier = function(facture) {

      var url = staticUrl + 'getDossier/' + $scope.user.latest_reg.id;

      $window.open(url);
    };


    angular.forEach(actRole, function (key, value) {
      console.log(value == 'Admin');
      if(value == 'Admin' || value == 'Trésorier' || value == 'Prof' || value == 'Permanent'){
        notOnly = true
      }
    });



    if(notOnly == true){

      Article.sorts.query(function(data){
        console.log(data);
        $scope.sorts = data;
      });

      $scope.getSub = function () {
        var total = 0;
        angular.forEach($scope.newVente.lines, function (key, value) {
          total += key.pu * key.quantite;
        })
        return total;
      };
      
      $scope.articles = [];
      
      $scope.getArticle = function(sortId, index){
        Article.articles.query({sort: sortId}, function(data){
          console.log(data);
          $scope.articles[index] = data;
        });
      };

      $scope.getDetailArticle = function (i, il) {
        $scope.newVente.lines[il].pu = $scope.articles[il][i].price;
        $scope.newVente.lines[il].realArticle = $scope.articles[il][i];
      };
      

      $scope.newVente = {
        remise: 0,
        lines : [
          {
            sort: "",
            article: "",
            realArticle: "",
            quantite: "",
            pu: "",
            total: ""
          }
        ]
      };


      $scope.addline = function () {
        $scope.newVente.lines.push({
          sort: "",
          article: "",
          realArticle: "",
          quantite: "",
          pu: "",
          total: ""
        });
      };

      $scope.registrationUpdate = {
        days: {},
        profs: {}
      };
      $scope.reducsUser = {};
      $scope.roleUser = {};
      $scope.showRatach = false;
      $scope.reafect = false;

      $scope.reafectOn = function () {
        $scope.reafect = true;
      };

      $scope.deleteUser = function () {
        Adhesion.role.delete({id: userId}, function (succes) {
          $state.go('app.index.adhesions');
        });
      };

      $scope.membersUrl = staticUrl + 'api/search_adherent?searchterm=';

      $scope.familyMembers = [];

      $scope.addMemberInFamily = function(user){
        console.log(user);
        if(user){
          $scope.familyMembers.push(user.originalObject);
          Adhesion.rattachement.save({regId: user.originalObject.latest_reg.id, familyId: familyId});
          return true;
        }
      };

      $scope.removeFromFamily = function(index, regId){

        $scope.familyMembers.splice(index,1);
        Adhesion.rattachement.remove({regId: regId});

      };

      $scope.affect = function () {
        Adhesion.updateUser.save({userId: userId, affecte: true}, function () {
          $scope.getUser();
        })
      };

      $scope.getCheckedValue = function(int){
        if(int == 1){
          return true;
        } else {
          return false;
        }
      };

      $scope.updateCheck = function(field){
        console.log('run updatecheck');
        field.id = regId;
        console.log(field);
        Adhesion.up.save(field, function(data){
          $scope.getUser();
        });
      };


      $scope.updateRole = function(field){
        console.log('run updatecheck');
        console.log(field);
        field.id = userId;
        Adhesion.role.save(field);
      };


      $scope.getUser = function(){
        Adhesion.one.get({id: userId},function(data){
          console.log(data.user);
          $scope.user = data.user;

          var reduc = data.user.latest_reg.reductions;
          var roles = data.user.roles;

          angular.forEach(reduc, function(key, value){
            if(key.id == 5){
              $scope.reducsUser[key.id] = true;
            } else {
              $scope.reducsUser[key.id] = key.id;
            }
          });

          angular.forEach(roles, function(key, value){
            console.log('id role', key);
            $scope.roleUser[key.id] = true;

          });

          console.log("role ", $scope.roleUser);

          console.log('reduc',$scope.reducsUser);

          regId = data.user.latest_reg.id;

          if(data.user.latest_reg.facture_id != null){
            $scope.factures = data.user.latest_reg.factures;
          } else {
            $scope.factures = null;
          }

          console.log(data.user.latest_reg.family_id);

          if(data.user.latest_reg.family_id != null){
            familyId = data.user.latest_reg.family_id;
            getFamilyMembers(data.user.latest_reg.family_id);
          }

          console.log(data.user);
          $scope.created_at = data.user.created_at;
        });
      };

;


      $scope.getUser();
      $scope.newPayement = {};

      $scope.getDateEch = function (date) {
        var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        $scope.newPayement.dateEch = birthdate;

      };

      $scope.depot = {};

      $scope.getDateDepot = function (date) {
        var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        $scope.depot.dDepot = birthdate;

      };

      $scope.depotDossier = function (){

        $scope.depot.id = $scope.user.latest_reg.id;
        $scope.depot.dossierDep = 1;
        Adhesion.update.save($scope.depot, function(result){
          $scope.getUser();
          console.log(result);
        }, function(err){
          console.error(err);
        });
      }


      $scope.getDateEnc = function (date) {
        var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
        $scope.newPayement.dateEnc = birthdate;

      };

      $scope.addPayement = function () {
        console.log($scope.factures.id);
        $scope.newPayement.factureId = $scope.factures.id;

        console.log($scope.newPayement);

        Facture.pay.save($scope.newPayement, function(success){
          $scope.factures = success.fac;
          $scope.newPayement.remarque = "";
          $scope.newPayement.amount = "";
          $scope.newPayement.methodId = "0";

        }, function(e){
          console.log(e);
        });

      };

      $scope.getTotalLines = function (){
        var total = 0;
        angular.forEach($scope.factures.lines, function(key, value){
          total += key.amount;
        })

        return total;

      };

      $scope.getTotalPay = function (){
        var total = 0;
        angular.forEach($scope.factures.payements, function(key, value){
          total += key.amount;
        });

        return total;

      };

      $scope.validateReg = function(){
        Adhesion.validate.save({id: regId}, function(data){
          $scope.getUser();
        });
      };

      $scope.removeFromPayement = function(id){
        //$scope.familyMembers.splice(index,1);
        Facture.pay.remove({payId: id}, function(success){
          $scope.factures = success.fac;
        }, function(e){
          console.log(e);
        });
      };





      $scope.createFamily = function(){
        $scope.showRatach = true;
        Adhesion.cFamily.save({regId: $scope.user.latest_reg.id}, function(data){
          console.log(data);
          familyId = data.familyId;
          $scope.familyMembers = data.users;
        })
      }

      Categorie.cat.get(function(data){
        console.log(data);
        $scope.categories = data.cat;
        $scope.profs = data.profs.users;
        $scope.reducs = data.reduc;
        $scope.groupes = data.groupes;
      });

      // récupére les jours liés à la categorie
      $scope.getDays = function(id){
        console.log(id);
        Categorie.day.get({id: id}, function(data){
          console.log(data.days[0].days);
          $scope.days = data.days[0].plannings;
        });
      };

      $scope.affectation = function(status){
        console.log( $scope.registrationUpdate);
        $scope.registrationUpdate.sid = status;
        $scope.registrationUpdate.id = $scope.user.latest_reg.id;
        Adhesion.aff.save($scope.registrationUpdate, function(result){
          $scope.reafect = false;
          $scope.getUser();
          console.log(result);
        }, function(err){
          console.error(err);
        });
      };

      $scope.createAvoir = function(id, payements){

        if($scope.getEncours(payements) <= 0){
          Facture.avoir.save({factureId: id, userId: userId}, function(success){
            $scope.getUser();
          }, function (error) {
            console.error(error);
          })
        } else {
          $state.go("app.index.payements", {factureId: id, userId: userId});
        }


      }

      $scope.setVente = function () {
        console.log($scope.newVente);
        $scope.newVente.userId = userId;
        Article.vente.save($scope.newVente, function (data) {
          $scope.newVente = {
            remise: 0,
            lines : [
              {
                sort: "",
                article: "",
                realArticle: "",
                quantite: "",
                pu: "",
                total: ""
              }
            ]
          };
          console.log('up user');
          $scope.getUser();
          console.log('up user end');

        })
      };

      $scope.saveRem = function(aff, lic){
        var remarque;
        if (aff.affectation == true){
          remarque = $scope.user.latest_reg.remarque_aff;
        } else {
          remarque = $scope.user.latest_reg.remarque;
        }


        aff.id = $scope.user.latest_reg.id;
        aff.rem = remarque;

        Adhesion.rem.save(aff);
      }

      $scope.saveLic = function(){
        var remarque;
        remarque = $scope.user.license;

        var aff = {};
        aff.id = userId;
        aff.lic = remarque;

        Adhesion.lic.save(aff);
      }

      $scope.inscriptionsForm = {

        ownTelContacts: [
          {tel: ""}
        ],
        ownEmailContacts: [
          {email: ""}
        ]
      };

      $scope.addTelOwnContact = function () {
        $scope.inscriptionsForm.ownTelContacts.push({tel: ""});
      };

      $scope.addEmailOwnContact = function () {
        $scope.inscriptionsForm.ownEmailContacts.push({email: ""});
      };

      $scope.createFacture = function(){
        Facture.facReg.save({reg: regId}, function(success){
          $scope.getUser();
          $scope.factures = success.fac
        }, function (error) {
          $scope.showFacError = true;
          console.error(error);
        })
      }



      // $scope.inscriptionsForm = {
      //   contacts: [
      //     {designation: "", tel: "", email: "", position: 1},
      //
      //
      //   ]
      // };
      //
      // $scope.today = new Date();
      //
      //
      // var nberContact = 0;
      //
      //
      //
      //
      //
      //
      //
      // $scope.submit = function () {
      //
      //   $scope.inscriptionsForm.age = $scope.age;
      //   console.log($scope.inscriptionsForm);
      //
      //   Inscription.save($scope.inscriptionsForm);
      // };
      //
      // $scope.addContact = function (date) {
      //   nberContact += 1;
      //   $scope.inscriptionsForm.contacts.push({designation: "", tel: "", email: "", position: nberContact});
      // };

    } else {
      Adhesion.my.get(function(data){
        console.log(data.user);
        $scope.user = data.user;

        var reduc = data.user.latest_reg.reductions;
        var roles = data.user.roles;

        angular.forEach(reduc, function(key, value){
          if(key.id == 5){
            $scope.reducsUser[key.id] = true;
          } else {
            $scope.reducsUser[key.id] = key.id;
          }
        });

        angular.forEach(roles, function(key, value){
          console.log('id role', key);
          $scope.roleUser[key.id] = true;

        });

        console.log("role ", $scope.roleUser);

        console.log('reduc',$scope.reducsUser);

        regId = data.user.latest_reg.id;

        if(data.user.latest_reg.facture_id != null){
          $scope.factures = data.user.latest_reg.factures;
        } else {
          $scope.factures = null;
        }

        console.log(data.user.latest_reg.family_id);

        if(data.user.latest_reg.family_id != null){
          familyId = data.user.latest_reg.family_id;
          getFamilyMembers(data.user.latest_reg.family_id);
        }

        console.log(data.user);
        $scope.created_at = data.user.created_at;
      })
    }

    function getFamilyMembers(id){
      $scope.showRatach = true;
      Adhesion.gFamily.get({familyId: id}, function(data){
        console.log(data);
        $scope.familyMembers = data.users;
      });


    }

  }]);
