'use strict';

angular.module('testUtcApp')
  .controller('DepotbanqueCtrl', function ($scope,  RoleStore, Facture) 
  {

      getCaisse();
      getDepot();
      $scope.compareIsLate = function (date) {
          var today = new Date();
          var d = new Date(date);
          if (d.getFullYear() <= today.getFullYear()){
              if(d.getMonth() < today.getMonth()){
                  return true
              } else {
                  return false
              }
          } else {
              return false
          }
      };

      $scope.chequeForDepot = {};
      $scope.dateDepot = null;
      $scope.amountEspece = null;
      $scope.totalOfCheque = 0;
      $scope.numberOfCheque = 0;
      
      $scope.makeDepot = function () {
          /*console.log($scope.amountEspece);
          return;*/
          Facture.depot.save({espece: $scope.amountEspece, cheques: $scope.chequeForDepot, date: $scope.dateDepot, remarque: $scope.remarque},
              function () {
                  $scope.chequeForDepot = {};
                  $scope.amountEspece = null;
                  $scope.dateDepot = null;
                  $scope.remarque = null;
                  $scope.totalOfCheque = 0;
                  $scope.numberOfCheque = 0;
                  getCaisse();
                  getDepot();
              });
      };

      $scope.getDate = function (date) {
          var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
          $scope.dateDepot = birthdate;

      };
      
      $scope.updateNumberOfcheque = function (item) {
          if($scope.chequeForDepot[item.id] !== false){
              $scope.numberOfCheque ++;
              $scope.totalOfCheque += item.amount;
          } else {
              $scope.numberOfCheque --;
              $scope.totalOfCheque -= item.amount;
          }
      };

      $scope.compareIsMonth = function (date) {
          var today = new Date();
          var d = new Date(date);
          if (d.getFullYear() == today.getFullYear()){
              if(d.getMonth() == today.getMonth()){
                  return true
              } else {
                  return false
              }
          } else {
              return false
          }
      };
///////////////////////////////////////////////////////
 //Règlements non déposés
    Facture.cheque.query(function(data){
      console.log(data);
      $scope.reglementsNonDep=data;
    });

    function getCaisse() {
        Facture.caisse.get(function (data) {
            console.log(data);
            $scope.totalEspece = data.sum;
            $scope.cheque = data.cheque;
            $scope.monthCheque = data.monthCheque;
            $scope.chequeLate = data.chequeLate;
        });
    }
      
    function getDepot() {
        Facture.depot.get(function (data) {
            $scope.depots = data.depots;
            $scope.$apply();
        });
    }

      $scope.getTotal = function (depot) {
          var total = depot.espece;

          total += $scope.getTotalCheque(depot);

          return total;
      };

      $scope.getTotalCheque = function (depot) {
          var total = 0;

          angular.forEach(depot.payements, function (obj) {
              total += obj.amount;
          });
          return total;
      };

      $scope.filtre = {};

      $scope.makeFiltre = function () {
          console.log($scope.filtre);
          Facture.depot.get($scope.filtre, function (data) {
              $scope.depots = data.depots
          });
      };
 //Règlements déposés
  Facture.pay.query({depot:1}, function(data){
                                              console.log(data);
                                              $scope.reglementsDep=data;
                                            });

  // Tous les règlements
      Facture.pay.query(function(data){
                                          console.log(data);
                                          $scope.reglements=data;

                                        });

      ///////  récupérer mntant total des espèces en caisse





  });