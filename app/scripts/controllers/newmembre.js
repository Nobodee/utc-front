'use strict';

angular.module('testUtcApp')
  .controller('NewmembreCtrl', function ($scope, Adhesion, envService, City) {

    $scope.age = null;
    var staticUrl = envService.read('staticUrl');
    $scope.citiesUrl = staticUrl + 'cities?searchterm=';

    // $scope.inscriptionsForm.auth_coor.unauthorized = false;

    $scope.inscriptionsForm = {
      ownTelContacts: [
        {tel: ""}
      ],
      ownEmailContacts: [
        {email: ""}
      ]
    };

    $scope.error = {
      empty: true
    };
    $scope.naissance = {};
    $scope.categories = [];
    $scope.isLocalReduc = 0;
    $scope.today = new Date();
    var nberContact = 2;

    $scope.getVille = function () {
      var villeId = $scope.ville;
      $scope.changeErrorState('ville');
      if(villeId){
        $scope.inscriptionsForm.ville = villeId;
      }
    };

    $scope.cities = [];

    $scope.codePostal = function(cp){
      if(cp.length >= 5) {
        City.cities.query({cp: cp}, function (cities) {
          console.log(cities);
          $scope.cities = cities;
        });
      }
    };

    $scope.uncheckedOther = function (unchecked){
      $scope.changeErrorState('auth_coor');
      if(unchecked && $scope.inscriptionsForm.auth_coor.unauthorized){
        $scope.inscriptionsForm.auth_coor = {
          unauthorized : true
        };
      } else if (unchecked && !$scope.inscriptionsForm.auth_coor.unauthorized && !$scope.inscriptionsForm.auth_coor.coor_p_fft && !$scope.inscriptionsForm.auth_coor.coor_p_tcu && !$scope.inscriptionsForm.auth_coor.coor_jr){
        $scope.inscriptionsForm.auth_coor.unauthorized = true;
      } else if (!unchecked && !$scope.inscriptionsForm.auth_coor.unauthorized && !$scope.inscriptionsForm.auth_coor.coor_p_fft && !$scope.inscriptionsForm.auth_coor.coor_p_tcu && !$scope.inscriptionsForm.auth_coor.coor_jr) {
        $scope.inscriptionsForm.auth_coor.unauthorized = true;
      } else {
        $scope.inscriptionsForm.auth_coor.unauthorized = false;
      }
      console.log($scope.inscriptionsForm.auth_coor);

    }

    $scope.changeErrorState = function(err){
      if(err in $scope.error){
        $scope.error[err] = false;
      }
    }

    $scope.submit = function () {

      $scope.isSubmit = true;
      $scope.error = {};
      var errorField = {};

      $scope.inscriptionsForm.age = $scope.age;
      console.log($scope.inscriptionsForm);


      Adhesion.all.save($scope.inscriptionsForm,
      function(data){
        $scope.isSubmit = false;
        $scope.inscriptionsForm = {
          ownTelContacts: [
            {tel: ""}
          ],
          ownEmailContacts: [
            {email: ""}
          ]
        };

      }, function(error){
        $scope.isSubmit = false;
        var err = error.data.error;
        console.log(err);
        if(err.prenom != null){
          errorField.prenom = 'Veuillez saisir votre nom et prénom  !';
        }
        if(err.nom != null){
          errorField.nom = 'Veuillez saisir votre nom !';
        }
        if(err.naissance != null){
          errorField.naissance = 'Veuillez sélectionner votre date de naissance !';
        }
        if(err.sexe != null){
          errorField.sexe = 'Veuillez choisir votre sexe !';
        }
        if(err.ville != null){
          errorField.ville = 'Veuillez selectioner votre ville !';
        }
        if(err['ownTelContacts.0.tel'] != null){
          errorField.tel = 'Veuillez saisir au moins un numéro de téléphone !';
        }
        if(err['ownEmailContacts.0.email'] != null){
          errorField.email = 'Veuillez saisir au moins une adresse email !';
        }
        if(err.auth_coor != null){
          errorField.auth_coor = 'Veuillez indiquer les autorisations concernant l\'usage de vos données personneles svp !';
        }
        if(err['reduction.id'] != null){
          errorField.reduction = 'Veuillez sélectionner une réduction svp !';
        }
        if(err['categorie.id'] != null){
          errorField.categorie = 'Veuillez choisir la formule souhaitée pour votre inscription svp !';
        }


        console.log(errorField);
        $scope.error = errorField;
        $scope.isSubmit = false;
      });
    };

    $scope.addContact = function (date) {
      nberContact += 1;
      $scope.inscriptionsForm.contacts.push({tel: "", lien_parente: "", nom: "", position: nberContact});
    };

    $scope.addTelOwnContact = function () {
      $scope.inscriptionsForm.ownTelContacts.push({tel: ""});
    };

    $scope.addEmailOwnContact = function () {
      $scope.inscriptionsForm.ownEmailContacts.push({email: ""});
    };

    $scope.getAge = function (date) {
      var birthdate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3"));
      console.log(birthdate);
      var d = new Date();
      var age = moment().diff(birthdate, 'years');
      console.log(age);
      $scope.age = age;
      $scope.inscriptionsForm.naissance = birthdate;

    };
  });
