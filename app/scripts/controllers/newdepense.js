'use strict';

angular.module('testUtcApp')
  .controller('NewdepenseCtrl', function ($scope, Facture, Account)
  {
	
	  $scope.depenseForm = {};
	  $scope.accountExist = true;
	  getAccount();

	  function getAccount(){
		  Account.accounts.query(function(data){
			  $scope.accounts = data;
		  });
	  }

	  $scope.newDepense = function () {
		  $scope.error = null;
		  console.log($scope.depense.$valid);
		  console.log($scope.depense);
		  if($scope.depense.$valid) {
			  Facture.depenses.save($scope.depenseForm, function (succes) {
				  $scope.depenseForm = {};
				  getAccount();
			  })
		  }
	  };

	  $scope.checkIfExist = function (number) {
		  var accounts = $scope.accounts;
		  var accountExist = false;
		  console.log(number);
		  angular.forEach(accounts, function (k) {
			  if(k.Numero == number){
				  accountExist = true;
				  $scope.depenseForm.accountTitre = k.Titre;
			  }
		  });
		  console.log("Account exist ? ", accountExist);
		  if(!accountExist){
			  $scope.depenseForm.accountTitre = "";
		  }
		  $scope.accountExist = accountExist;
	  }

  });