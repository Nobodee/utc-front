'use strict';


  angular.module('testUtcApp', [
  'ngResource',
  'ui.router',
  '720kb.datepicker',
  'angularMoment',
  'angucomplete-alt',
  'satellizer',
  'permission',
  'ui.bootstrap',
  'permission.ui',
  'environment',
  'ngCookies',
  'ngLoadingSpinner',
  'ngFileSaver'
])
  .config(function(envServiceProvider, $authProvider) {
    envServiceProvider.config({
			domains: {
				development: ['127.0.0.1:9000'],
				production: ['http://webasso.fr', 'http://www.webasso.fr', 'www.webasso.fr', 'webasso.fr', 'tennisclubdelunion.com', 'http://www.tennisclubdelunion.com', 'www.tennisclubdelunion.com', 'localhost:8000']
			},
			vars: {
				development: {
          //Rodrigue décommente la prochaine ligne et commente la suivante
        //apiUrl: 'http://tennisclubdelunion.com/',
           apiUrl: 'http://localhost\\:8000/',
          //Rodrigue la prochaine ligne et commente la suivante
        //staticUrl: 'http://tennisclubdelunion.com/'
          staticUrl: 'http://localhost:8000/'
				},
				production: {
					apiUrl: '/',
          staticUrl: '/'
				}
			}
		});

		// run the environment check, so the comprobation is made
		// before controllers and services are built
		envServiceProvider.check();

    var apiUrl = envServiceProvider.$get().read('staticUrl');
    $authProvider.loginUrl = apiUrl + 'api/authenticate';


  })

  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];


    $httpProvider.interceptors.push(['$q', '$location', function($q, $location) {
      return {
        'response': function(response) {
          if (response.status === 400) {
              console.log("Response Error 400",response);
              $location.path('/login');
          }
          return response;
        },
        responseError: function(rejection) {
            if (rejection.status === 400) {
              console.log("Response Error 400",rejection);
              $location.path('/login');
            }
            return $q.reject(rejection);
        }
      };
    }]);


    $stateProvider
      // .state('index', {
      //   url: '/',
      //   templateUrl: 'views/main.html',
      //   controller:'MainCtrl'
      // })

      .state('app',{
        url: '',
        abstract: true,
        controller: 'AppCtrl'
      })

      .state('app.index',{
        url: '/',
        abstract: true,
        views:{

          'mainContainer@':{
            templateUrl: 'views/Common/index.html'
          },
          'navbar@app.index':{
            templateUrl: 'views/Common/navbar.html'

          }
        }
      })







      .state('myAccount', {
        url: '/moncompte',
        templateUrl: 'views/myAccount.html',
        controller: 'MyaccountCtrl'
      })

      .state('app.auth', {
        url: '/login',
        views:{
          'mainContainer@': {
            templateUrl: 'views/auth.html'
           // controller: 'AuthCtrl'
          }
        }
      })

      .state('app.forgetPwd', {
        url: '/forgetPwd',
        views: {
          'mainContainer@': {
            templateUrl: 'views/forgetPwd.html',
            controller: 'ForgetpwdCtrl'
          }
        }
      })


 //Liste adhésions
      .state('app.index.adhesions', {
        url: 'adhesions',
        views: {
          'content@app.index': {
            templateUrl: 'views/adhesions.html',
            controller: 'AdhesionsCtrl'
          }
        }
      })

//Nouvelle adhésion via inscription & formulaire
    .state('inscription', {
        url: '/inscription',
        views:{
          'mainContainer@': {
            templateUrl: 'views/inscription.html',
            controller: 'InscriptionCtrl'
          }
        }
      })


//Liste adhérents
      .state('app.index.adherents', {
        url: 'adherents',
        views: {
          'content@app.index':{
            templateUrl: 'views/adherents.html',
            controller: 'AdherentsCtrl'
          }
        }

      })

// Fiche adhérent
      .state('app.index.adherent', {
        url: 'adherent/:userId',
        views: {
          'content@app.index': {
            templateUrl: 'views/adherent/adherent.html',
            controller: 'AdherentCtrl'
          },
          'info@app.index.adherent':{
            templateUrl: 'views/adherent/info.html'
          },
          'affectation@app.index.adherent':{
            templateUrl: 'views/adherent/affectation.html'
          },
          'permanence@app.index.adherent':{
            templateUrl: 'views/adherent/permanence.html'
          },
          'facture@app.index.adherent':{
            templateUrl: 'views/adherent/facture.html'
          },
          'role@app.index.adherent':{
            templateUrl: 'views/adherent/role.html'
          }
        }
      })

      .state('app.index.edit', {
        url: 'edit/:userId',
        views: {
          'content@app.index':{
            templateUrl: 'views/edit.html',
            controller: 'EditCtrl'
          }
        }
      })


// liste des recettes
      .state('app.index.recettes', {
        url: 'recettes',
        views: {
          'content@app.index': {
            templateUrl: 'views/recettes.html',
            controller: 'RecettesCtrl'
          }
        }
      })
// Nouvelle vente hors inscription
       .state('app.index.newvente', {
        url: 'newvente',
        views: {
          'content@app.index': {
            templateUrl: 'views/newvente.html',
            controller: 'NewventeCtrl'
          }
        }
      })

// Nouvelle recette sans vente et inscription :subvention, dons , autres...
       .state('app.index.newrecette', {
        url: 'newrecette',
        views: {
          'content@app.index': {
            templateUrl: 'views/newrecette.html',
            controller: 'NewrecetteCtrl'
          }
        }
      })


// Enregistrer une nouvelle dépense
        .state('app.index.newdepense', {
        url: 'newdepense',
        views: {
          'content@app.index': {
            templateUrl: 'views/newdepense.html',
            controller: 'NewdepenseCtrl'
          }
        }
      })
// Suivi des dépenses
        .state('app.index.depenses', {
        url: 'depenses',
        views: {
          'content@app.index': {
            templateUrl: 'views/depenses.html',
            controller: 'DepensesCtrl'
          }
        }
      })

// Trésorerie / déposer à la banque chèque espèces
   .state('app.index.depotbanque', {
        url: 'depotbanque',
        views: {
          'content@app.index': {
            templateUrl: 'views/depotbanque.html',
            controller: 'DepotbanqueCtrl'
          }
        }
      })
       
        .state('app.index.depotbanque.new', {
          url: '/nouveau',
          views: {
            'page': {
              templateUrl: 'views/newDepot.html',
              controller: 'DepotbanqueCtrl'
            }
          }
        })

        .state('app.index.depotbanque.historique', {
          url: '/historique',
          views: {
            'page': {
              templateUrl: 'views/historiqueDepot.html'
            }
          }
        })

 // Trésorerie / suivi de la caisse
        .state('app.index.echeancier', {
        url: 'echeancier',
        views: {
          'content@app.index': {
            templateUrl: 'views/echeancier.html',
            controller: 'EcheancierCtrl'
          }
        }
      })

// Gestion / Relances

        .state('app.index.relances', {
        url: 'relances',
        views: {
          'content@app.index': {
            templateUrl: 'views/relances.html',
            controller: 'RelancesCtrl'
          }
        }
      })

// Grand Livre Recettes-Dépenses
        .state('app.index.grandlivre', {
        url: 'grandlivre',
        views: {
          'content@app.index': {
            templateUrl: 'views/grandlivre.html',
            controller: 'GrandlivreCtrl'
          }}
      })


// Paramétrage / créer membre sans inscription
        .state('app.index.newmembre', {
        url: 'newmembre',
        views: {
          'content@app.index': {
            templateUrl: 'views/newmembre.html',
            controller: 'NewmembreCtrl'
          }
        }
      })

// Paramétrage / Gestion adhérents : suppression /archivage /ré-activation/Rôles
        .state('app.index.gestionadherents', {
        url: 'gestionadherents',
        views: {
          'content@app.index': {
            templateUrl: 'views/gestionadherents.html',
            controller: 'GestionadherentsCtrl'
          }
        }
      })
// paramétrage / definir periode saison
        .state('app.index.saison', {
        url: 'saison',
        views: {
          'content@app.index': {
            templateUrl: 'views/saison.html',
            controller: 'SaisonCtrl'
          }
        }
      })



// paramétrage / definir periode renouvellement
        .state('app.index.renouvellement', {
        url: 'renouvellement',
        views: {
          'content@app.index': {
            templateUrl: 'views/renouvellement.html',
            controller: 'RenouvellementCtrl'
          }
        }
      })

// paramétrage / definir periode carte été
        .state('app.index.carteete', {
        url: 'carteete',
        views: {
          'content@app.index': {
            templateUrl: 'views/carteete.html',
            controller: 'CarteeteCtrl'
          }
        }
      })

// paramétrage / gestion articles
        .state('app.index.articles', {
        url: 'articles',
        views: {
          'content@app.index': {
            templateUrl: 'views/articles.html',
            controller: 'ArticlesCtrl'
          }
        }
      })
            
  // paramétrage / gestion compte comptable
        .state('app.index.account', {
          url: 'account',
          views: {
            'content@app.index': {
              templateUrl: 'views/account.html',
              controller: 'AccountCtrl'
            }
          }
        })
        
      .state('app.index.payements', {
        url: 'payements/:factureId/:userId',
        views: {
          'content@app.index': {
            templateUrl: 'views/payements.html',
            controller: 'PayementsCtrl'
          }
        }
      });
    
    $urlRouterProvider.otherwise('/login');

  })

  .run(function ($cookies, RoleStore, amMoment) {

    amMoment.changeLocale('fr');

    var role = $cookies.getObject('role');

      console.log(role);
    if(role != null){
      RoleStore.clearStore();
      angular.forEach(role, function(key, value){
        RoleStore.defineRole(key.name, []);
      });
      var actRole = RoleStore.getStore();
      console.log(actRole);
    }

  });
